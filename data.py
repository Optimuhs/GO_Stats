from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from sportsApp import app, db
from app.models import Team, Player
from datetime import datetime


def updated_stats(driver):
    #Get updated ranking list, if it exists
    new_link = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "div.filter-column-content > a"))).get_attribute('href')
    if new_link != start:
        driver.get(new_link)

        header = driver.find_elements(By.CSS_SELECTOR,"div.regional-ranking-header")
        team_n = driver.find_elements(By.CSS_SELECTOR,"span.name") #Team name
        team_position = driver.find_elements(By.CSS_SELECTOR,"span.position") #Team global ranking, for now, updated manually each release of ranking
        team_roster = driver.find_elements(By.CSS_SELECTOR,"div.playersLine") #Team roster
        
        #Seperate search for team at rank 1, JS interferes with only this position
        rank1_team = driver.find_elements(By.CSS_SELECTOR,"table.lineup")
        rank1_team = rank1_team[0].text.split()

        if check_teams(team_n) == True:
            delete_db()
            add_team(team_n, team_position, team_roster, rank1_team)

#Create team and player objects 
#Add them to db
def add_team(team_n, team_position, team_roster, rank1_team):
    with app.app_context():
        for i in range(len(team_n)):
            team_r = team_roster[i].text.split() #Get str from webelement
            tp = team_position[i].text #Get int from webelement
            t = Team(team_name=team_n[i].text, team_rank=int(tp[1:]))
            
            db.session.add(t)
            db.session.flush()
            add_players(team_r, t, rank1_team)
     

#Takes team rank and id
def add_players(team_r,t,rank1_team):
    #First place not appearing because of JavaScript
    if len(team_r) == 0:
        p1 = Player(game_name=rank1_team[0], team_id=t.id, team_name = t.team_name)
        p2 = Player(game_name=rank1_team[1], team_id=t.id, team_name = t.team_name)
        p3 = Player(game_name=rank1_team[2], team_id=t.id, team_name = t.team_name)
        p4 = Player(game_name=rank1_team[3], team_id=t.id, team_name = t.team_name)
        p5 = Player(game_name=rank1_team[4], team_id=t.id, team_name = t.team_name)
        db.session.add(p1)
        db.session.add(p2)
        db.session.add(p3)
        db.session.add(p4)
        db.session.add(p5)
        db.session.commit()
    #Other 49 are ok
    else:
        for i in range(len(team_r)):
            p1 = Player(game_name=team_r[i], team_id=t.id, team_name = t.team_name)
            db.session.add(p1)
        db.session.commit()

#Clear db so we can repopulate with new data
def delete_db():
    with app.app_context():
        team = db.session.query(Team).all()
        player = db.session.query(Player).all()
        #If db is not empty
        if len(team) > 0 and len(player) > 0:
            for i in team:
                db.session.delete(i)
                db.session.commit()
            for j in player:
                db.session.delete(j)
                db.session.commit()
        
#Check to see if data scraped matches data in db
def check_teams(new_teams):
    with app.app_context():
        teams = db.session.query(Team).order_by(Team.team_rank.asc()).all()
        #If db is not empty
        if len(teams) > 0:
            count = 0
            #Check if the team rank order matches the updated rank order
            for i in teams:
                if i.team_name == new_teams[teams.index(i)]:
                    count += 1
            #If all teams match, no changes were made
            if count == len(teams):
                return False
            else:
                return True
        else:
            return True


if __name__ == "__main__":
    start = "https://www.hltv.org/ranking/teams/2020/october/5"
    driver = webdriver.Firefox(executable_path="D:/geckodriver.exe")
    driver.get(start)
    updated_stats(driver)