from datetime import datetime, timedelta
from app import create_app, db
import unittest
from app.models import User, Posts, Player, Team
from config import Config

class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'

class UserModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def teadDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_password_hashing(self):
        user = User(username='Susan')
        user.set_password('Susan123')
        self.assertFalse(user.check_password('susan123'))
        self.assertTrue(user.check_password('Susan123'))
      
    def test_follow(self):
        u1 = User(username='Dave')
        u2 = User(username='John')
        db.session.add(u1)
        db.session.add(u2)
        db.session.commit()
        self.assertEqual(u1.followers.all(), [])
        self.assertEqual(u1.followed.all(), [])

        u1.follow(u2)
        u2.follow(u1)
        db.session.commit()
        self.assertTrue(u1.is_following(u2))
        self.assertTrue(u2.is_following(u1))
        self.assertEqual(u1.followers.count(), 1)
        self.assertEqual(u1.followed.first().username, 'John')
        self.assertTrue(u2.followed.count(), 1)
        self.assertEqual(u2.followers.first().username, 'Dave')
       
        u1.unfollow(u2)
        db.session.commit()
        self.assertFalse(u1.is_following(u2))
        self.assertEqual(u1.followers.count(), 1    )
        self.assertTrue(u2.is_following(u1))
        self.assertTrue(u2.followed.count(), 0)

 
    def test_posts(self):
        u1 = User(username='Joe', email = 'joe@example.com')
        u2 = User(username='Bob', email = 'bob@example.com')
        u3 = User(username='Roger', email='roger@example.com')
        u4 = User(username='Cait', email='cait@example.com')
        db.session.add_all([u1,u2,u3,u4])

        now = datetime.utcnow()
        p1 = Posts(body="Im Joe", author=u1, timestamp=now + timedelta(seconds=1))
        p2 = Posts(body="Im Bob", author=u2, timestamp=now + timedelta(seconds=4))
        p3 = Posts(body="Im Roger", author=u3, timestamp=now + timedelta(seconds=3))
        p4 = Posts(body="Im Cait", author=u4, timestamp=now + timedelta(seconds=2))
        db.session.add_all([p1,p2,p3,p4])
        db.session.commit()

        u1.follow(u2)
        u1.follow(u3)
        u2.follow(u4)
        u3.follow(u1)
        u4.follow(u3)
        db.session.commit()

        f1 = u1.followed_posts().all()
        f2 = u2.followed_posts().all()
        f3 = u3.followed_posts().all()
        f4 = u4.followed_posts().all()
        self.assertEqual(f1, [p2,p3,p1])    
        self.assertEqual(f2, [p2,p4])  
        self.assertEqual(f3, [p3,p1])
        self.assertEqual(f4, [p3,p4])       

    #Create a fake team and commit to db
    #Test to ensure unique id is assigned and is added to db
    def test_team(self):
        team1 = Team(team_name='teamName')
        team2 = Team(team_name='team2Name')
        db.session.add_all([team1, team2])
        db.session.commit()
        
        t1 = Team.query.filter_by(team_name='teamName').first()
        t1 = [t1.team_name]
       
        self.assertEqual(team1.team_name, t1[0]) 
        self.assertFalse(team1==team2)
        self.assertFalse(team1.id==team2.id)

    #Create a fake team roster and commit it to the db
    #Test to ensure players are correctly assuming the (team:player) relationship
    def test_player(self):
        team3 = Team(team_name='team3Name')
        team4 = Team(team_name='team4Name')

        db.session.add_all([team3,team4])
        db.session.commit()

        #Team1 roster
        tp1 = Player(game_name='AName', team_id=team3.id)
        tp2 = Player(game_name='BName', team_id=team3.id)
        tp3 = Player(game_name='CName', team_id=team3.id)
        tp4 = Player(game_name='DName', team_id=team3.id)
        tp5 = Player(game_name='EName', team_id=team3.id)

        #Team2 roster
        tp6 = Player(game_name='FName', team_id=team4.id)
        tp7 = Player(game_name='HName', team_id=team4.id)
        tp8 = Player(game_name='IName', team_id=team4.id)
        tp9 = Player(game_name='JName', team_id=team4.id)
        tp10 = Player(game_name='KName', team_id=team4.id)  
        
        
        db.session.add_all([tp1,tp2,tp3,tp4,tp5,tp6,tp7,tp8,tp9,tp10])
        db.session.commit()

        #different query syntax, does the same thing
        team3_roster = db.session.query(Player).filter_by(team_id=team3.id).all()
        team4_roster = Player.query.filter_by(team_id=team4.id).all()
        
        
        self.assertFalse(team3_roster==team4_roster)
        self.assertFalse(team3_roster[1]==team4_roster[1])
        self.assertTrue([tp1,tp2,tp3,tp4,tp5]==team3_roster)
        self.assertFalse([tp1,tp2,tp3,tp4,tp5]==team4_roster)
      
if __name__ == '__main__':
    unittest.main(verbosity=2)