import os
from dotenv import load_dotenv
basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

 

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'simple_is_number_one'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DB_STRING', 'postgres://postgres:password@localhost:5432/gostats')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    POSTS_PER_PAGE = 15
    TEAMS_PER_PAGE = 15
    PLAYERS_PER_PAGE = 15
    #Email errors

    MAIL_SERVER = os.environ.get('MAIL_SERVER')# 'smtp.googlemail.com'
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or 'parkerjosh468@gmail.com'
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or '2056953jp'
    ADMINS = ['parkerjosh468@gmail.com']
    SECURITY_EMAIL_SENDER = 'parkerjosh468@gmail.com'

    #Define to be able to work on app without service being up
    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')

    #API 
    MY_KEY = os.environ.get('MY_KEY')