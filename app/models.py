from app import db
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from flask import current_app
from app import login
from time import time
import jwt
from app.search import add_to_index, remove_from_index, query_index

class SearchableMixin(object):
    #returns result ids and total number
    #cls used to make index name the table name
    @classmethod
    def search(cls, expression, page, per_page): #passing class and not instance
        ids, total = query_index(cls.__tablename__, expression, page, per_page)
        if total == 0:
            return cls.query.filter_by(id=0), 0
        when = []
        for i in range(len(ids)):
            when.append((ids[i], i))
        return cls.query.filter(cls.id.in_(ids)).order_by(
            db.case(when, value=cls.id)), total

    #Save added, modified, and deleted objects before they are deleted by the commit
    @classmethod
    def before_commit(cls, session):
        session._changes = {
            'add' : list(session.new),
            'update' : list(session.dirty),
            'delete' : list(session.deleted)
        }

    #Loop through dictionary and update Elasticsearch index
    @classmethod
    def after_commit(cls, session):
        for obj in session._changes['add']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['update']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['delete']:
            if isinstance(obj, SearchableMixin):
                remove_from_index(obj.__tablename__, obj)
        session._changes = None

    #refresh index with all data from relational side
    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)

#Event handler setup
db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)




#Association table
followers = db.Table('followers',
        db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
        db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
    )

#Create user table
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Posts', backref='author', lazy='dynamic')
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    verified = db.Column(db.Boolean, default=False)

    #Create password hash using werkzeug
    def set_password(self, password):
        self.password_hash = generate_password_hash(password) 
    
    #Check password hash matches password
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    #Verify user email
    def verification_token(self, expires_in=(time)):
        return jwt.encode(
            {'verify_account': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def verification_token_check(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
            algorithms=['HS256'])['verify_account']

        except:
            return
        return User.query.get(id)

    #Password Reset
    def passwordreset_token(self, expires_in=(time)):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def passwordreset_token_check(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
            algorithms=['HS256'])['reset_password']

        except:
            return
        return User.query.get(id)

    #Referencing User, using followers association table
    #Left side of relationship
    #Right side of relationship,
    #Reference from right side, query settings
    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'), lazy='dynamic'
    )

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0
    
    #Query post and followers tables, join them into a temporary table
    #Filter posts based on followed status 
    #Query for users own posts
    #Combine and return them in order by datetime posted. 
    def followed_posts(self):
        followed = Posts.query.join(
            followers, (followers.c.followed_id == Posts.user_id)).filter(
                followers.c.follower_id == self.id)
                
        own = Posts.query.filter_by(user_id=self.id)        
        return followed.union(own).order_by(Posts.timestamp.desc())

          

    def __repr__(self):
        return 'User {}'.format(self.username)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

#Create posts tables
class Posts(SearchableMixin, db.Model):
    __searchable__ = ['body']
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Post {}>'.format(self.body)

class Team(SearchableMixin, db.Model):
    __searchable__ = ['team_name']
    id = db.Column(db.Integer, primary_key=True)
    team_name = db.Column(db.String(140), index=True, unique=True)
    team_roster = db.relationship('Player', backref='member')
    team_rank = db.Column(db.Integer, index=True)

    def __repr__(self):
        return  '<Team {}>'.format(self.team_name)

#'<ProTeam {}>'.format(self.team_name) or
#create player tables
class Player(SearchableMixin, db.Model):
    __searchable__ = ['game_name']
    id = db.Column(db.Integer, primary_key=True)
    game_name = db.Column(db.String(140), unique=True, index=True)
    team_name = db.Column(db.String(140))
    team_id = db.Column(db.Integer, db.ForeignKey('team.id'))

    def __repr__(self):
        return '<Player {}>'.format(self.game_name)

