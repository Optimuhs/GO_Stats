from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, current_app, g
from flask_login import current_user, login_required
from app import db
from app.main.forms import EditProfile, EmptyForm, PostForm, SearchForm
from app.models import User, Posts, Team, Player
from app.main import bp

@bp.route('/', methods=['GET', 'Post'])
@bp.route('/index', methods=['GET', 'Post'])
@login_required
def index():
    form = PostForm()
    if form.validate_on_submit():
        post = Posts(body=form.post.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('Your post is now live!')
        return redirect(url_for('main.index'))
    page = request.args.get('page', 1, type=int)
    posts = current_user.followed_posts().paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.index', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.index', page=posts.prev_num) \
        if posts.has_prev else None 

    return render_template('index.html', title='Home', posts=posts.items, form=form, next_url=next_url, prev_url=prev_url)



@bp.route('/user/<username>', methods=['GET', 'POST'])
@login_required
def profile(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = current_user.posts.order_by(Posts.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.profile', username=user.username, page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.profile', username=user.username, page=posts.prev_num) \
        if posts.has_prev else None
    form = EmptyForm()
    return render_template('profile.html', title=(str(user.username)+"'s Account"), user=user, posts=posts.items, form=form, next_url=next_url, prev_url=prev_url)

@bp.route('/editprofile', methods=['GET', 'POST'])
@login_required
def editprofile():
    form = EditProfile( current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Changes have been saved')
        return redirect(url_for('main.editprofile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('editprofile.html', title='Edit Profile', form=form)


@bp.route('/follow/<username>', methods=['POST'])
@login_required
def follow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        if user is None:
            flash('User {} not found'.format(username))
            return redirect(url_for('main.index'))
        if user == current_user:
            flash('You cannot follow yourself')
            return redirect(url_for('main.profile', username=username))
        current_user.follow(user)
        db.session.commit()
        flash('Now following {}!'.format(username))
        return redirect(url_for('main.profile', username=username))
    else:
        return redirect(url_for('main.index'))

@bp.route('/unfollow/<username>', methods=['POST'])
@login_required
def unfollow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        if user is None:
            flash('User {} not found'.format(username))
            return redirect(url_for('main.index'))
        if user == current_user:
            flash('You cannot unfollow yourself')
            return redirect(url_for('main.profile'))
        current_user.unfollow(user)
        db.session.commit()
        flash('You are not following {}.'.format(username))
        return redirect(url_for('main.profile',username=username))
    else:
        redirect(url_for('main.index'))

@bp.route('/explore')
@login_required
def explore():
    page = request.args.get('page', 1, type=int)
    posts = Posts.query.order_by(Posts.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.explore', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.explore', page=posts.prev_num) \
        if posts.has_prev else None
    return render_template('index.html', title='Explore', posts=posts.items, next_url=next_url, prev_url=prev_url)

@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()

@bp.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.explore'))
    page = request.args.get('page', 1, type=int)
    #Query for each model
    posts, total_post = Posts.search(g.search_form.q.data, page, current_app.config['POSTS_PER_PAGE'])
    player, total_player = Player.search(g.search_form.q.data, page, current_app.config['PLAYERS_PER_PAGE'])
    team, total_team = Team.search(g.search_form.q.data, page, current_app.config['TEAMS_PER_PAGE'])        
    result = sum([total_post, total_player, total_team])
    #If no matches redirect to index
    if result == 0:
        flash("Please check your spelling")
        return redirect(url_for('main.index'))
    next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
        if total_post > page * current_app.config['POSTS_PER_PAGE'] or total_team > page * current_app.config['TEAMS_PER_PAGE'] or total_player > page * current_app.config['PLAYERS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title = ('Search'), player=player, team=team, posts=posts, next_url=next_url, prev_url=prev_url)


@bp.route('/team/', defaults={'id':None})
@bp.route('/team/<id>', methods=['GET', 'POST'])
def team(id):
    if id is None:
        t = db.session.query(Team.team_name, Team.id).order_by(Team.team_rank.asc()).all()
        p = None
    else:
        t = Team.query.filter_by(id=id).first()
        t = [t.team_name, t.id]
        p = Player.query.filter_by(team_id=id).all()
        redirect(url_for('main.team', team_data=t, team_roster=p))

    return render_template('team.html', team_data=t, team_roster=p, id=id, title="Current CSGO Global Rankings")

@bp.route('/player/', defaults={'id':None})
@bp.route('/player/<id>', methods=['GET', 'POST'] )
def player(id):
    if id is None:
        redirect(url_for('main.team'))
    else:
        p = Player.query.filter_by(id=id).first()
        p_stats = [p.game_name, p.team_name]
        return render_template('player.html', player=p_stats, title=p.game_name)