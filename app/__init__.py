from flask import Flask, request, current_app
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
import logging 
from logging.handlers import SMTPHandler, RotatingFileHandler
from flask_mail import Message, Mail
from elasticsearch import Elasticsearch
import os

#Instances 

db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()
login.login_view = 'auth.login'
mail = Mail()


def create_app(config_class=Config):
    sports_app = Flask(__name__)
    sports_app.config.from_object(config_class)

    db.init_app(sports_app)
    migrate.init_app(sports_app, db)
    login.init_app(sports_app)
    mail.init_app(sports_app)
  
    #Make instance none if URL not define for environment 
    sports_app.elasticsearch = Elasticsearch([sports_app.config['ELASTICSEARCH_URL']]) \
        if sports_app.config['ELASTICSEARCH_URL'] else None

    #import blueprints
    from app.errors import bp as errors_bp
    sports_app.register_blueprint(errors_bp)

    from app.auth import bp as auth_bp
    sports_app.register_blueprint(auth_bp, url_prefix='/auth')

    from app.main import bp as main_bp
    sports_app.register_blueprint(main_bp)

    if not sports_app.debug:
        #Email errors
        if sports_app.config['MAIL_SERVER']:
            auth = None
            if sports_app.config['MAIL_USERNAME'] or sports_app.config['MAIL_PASSWORD']:
                auth = (sports_app.config['MAIL_USERNAME'], sports_app.config['MAIL_PASSWORD'])
            secure = None
            if sports_app.config['MAIL_USE_TLS']:
                secure = ()
            mail_handler = SMTPHandler(
                mailhost = (sports_app.config['MAIL_SERVER'], sports_app.config['MAIL_PORT']),
                fromaddr = 'no-reply@' + sports_app.config['MAIL_SERVER'],
                toaddrs = sports_app.config['ADMINS'],  subject='GO-Stats Fail',
                credentials=auth, secure=secure)
            mail_handler.setLevel(logging.ERROR)
            sports_app.logger.addHandler(mail_handler)

        #Log errors
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler('logs/GO_Stats.log', maxBytes=10240,
                                        backupCount=10)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
        file_handler.setLevel(logging.INFO)
        sports_app.logger.addHandler(file_handler)

        sports_app.logger.setLevel(logging.INFO)
        sports_app.logger.info('GO-Stats startup')
    return sports_app

from app import models

