from flask import render_template, current_app
from app.email import send_email

def send_verification_email(user):
    token = user.verification_token(60*4320)
    send_email('Account Verification', 
                sender = current_app.config['ADMINS'][0],
                recipients = [user.email] ,  
                text_body = render_template('email/verification.txt', user=user, token=token))

def send_passwordreset_email(user):
    token = user.passwordreset_token(60* 1440)
    send_email('Password Reset',
                sender = current_app.config['ADMINS'][0],
                recipients = [user.email],
                text_body = render_template('email/passwordreset.txt', user=user, token=token))