from app import db
from app.auth import bp
from flask import render_template, flash, redirect, url_for, request
from app.auth.forms import LoginForm, Registration, PasswordResetForm, RequestPasswordResetForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User
from werkzeug.urls import url_parse
from datetime import datetime
from app.auth.email import send_verification_email, send_passwordreset_email


@bp.route('/login', methods = ['GET', 'POST'])
def login():
    #If logged in user opens login page, redirect to index
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    #Log user in
    #Query for username and redirect to the appropriate page
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('auth.login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        #Check for next argument if not redirect to index
        #Check if next argument is set to relative path, redirect to url
        #Check if next argument is set to URL that includes domain name, redirect to index
        #Use netloc check if url is relative
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main.index')
        return redirect(next_page)
    return render_template('auth/login.html', title='Sign In', form=form)

@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@bp.route('/register', methods=['GET', 'POST'])
def register():
    #if user already logged in
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = Registration()
    #if user making new account
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        send_verification_email(user)
        flash('Registration successful, check email for confirmation')
        return redirect(url_for('auth.login'))
    return render_template('auth/register.html', title='Register', form=form)



@bp.route('/verify/<token>', methods=['GET', 'POST'])
def verification(token):
    user = User.verification_token_check(token)
    if not user:
        return redirect(url_for('main.index'))
    user.verified = True
    db.session.commit()
    flash('Your account has been verified.')
    return render_template('auth/verification.html', user=user)


@bp.route('/request_passwordreset', methods=['GET', 'POST'])
def request_passwordreset():
    if current_user.is_authenticated:
        redirect(url_for('main.index'))
    form = RequestPasswordResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_passwordreset_email(user)
            flash('Your password reset email has been sent.')
            return redirect(url_for('auth.login'))
        else:
            return redirect(url_for('auth.request_passwordreset'))
    return render_template('auth/request_passwordreset.html', form=form)

@bp.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    user = User.passwordreset_token_check(token)
    if not user:
        return redirect(url_for('main.index'))
    form = PasswordResetForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset')
        return redirect(url_for('auth.login'))
    return render_template('auth/reset_password.html', form=form)
    
