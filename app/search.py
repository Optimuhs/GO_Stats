from flask import current_app

#All check for elasticsearch server
#If none found, continue running the application without errors

#Takes SQLAlchemy model as second arg
def add_to_index(index, model):
    if not current_app.elasticsearch:
        return
    payload = {}
    for field in model.__searchable__:
        payload[field] = getattr(model, field)
    current_app.elasticsearch.index(index=index, id=model.id, body=payload)

def remove_from_index(index, model):
    if not current_app.elasticsearch:
        return
    current_app.elasticsearch.delete(index=index, id=model.id)

#Returns list of elements matching id and total number of results
#search 
def query_index(index, query, page, per_page): #model, 
    if not current_app.elasticsearch:
        return [], 0
    search = current_app.elasticsearch.search(
        index=index, 
        body={'query': {'multi_match': {'query': query, 'fields': ['game_name', 'team_name', 'body']}}, #
            'from': (page - 1) * per_page, 'size' : per_page})
    ids = [int(hit['_id']) for hit in search['hits']['hits']]
    return ids, search['hits']['total']['value']
            