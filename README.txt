#Welcome to GO-Stats

Here you can create an account, subscribe to view the stats of your favorite teams, and follow your friends. 
Still in progress

#Installation
Must have Python 3 pre-installed (Windows)

'''' Create virtual environment (in cmd at wanted directory) if not using Docker ''''
$python3 -m venv <env name> 

''''Activate virtual env in path of virtual env ''''
$<env name>/scripts/activate

'''' In CMD use package manager (pip) to install the dependencies''''
pip install -r requirements.txt

#Usage
''''Activate virtual env in directory of virtual env ''''
$<env name>/scripts/activate

''''Activate flask application (in flask app directory)''''
$flask run

''''If command above does not work ''''
Mac: 
$export FLASK_APP=sportsApp.py

Windows:
$set FLASK_APP=sportsApp.py

Try flask run again, in flask application directory

''''In browser''''
http://localhost:5000/

#Contributing
Contributions are welcome.
Ensure to create an issue for major changes.
Comment on commits.

#License
MIT
