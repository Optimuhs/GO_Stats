"""update player and team models

Revision ID: b85a130685b2
Revises: 2a5b84bdf5f8
Create Date: 2020-10-12 13:54:02.263960

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b85a130685b2'
down_revision = '2a5b84bdf5f8'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('player', sa.Column('team_name', sa.String(length=140), nullable=True))
    op.add_column('team', sa.Column('team_rank', sa.Integer(), nullable=True))
    op.create_index(op.f('ix_team_team_rank'), 'team', ['team_rank'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_team_team_rank'), table_name='team')
    op.drop_column('team', 'team_rank')
    op.drop_column('player', 'team_name')
    # ### end Alembic commands ###
