from app import create_app, db
from app.models import User, Posts

app = create_app()

#Give flask shell context 
#Returns dict that provices name (as key) and reference (value)
@app.shell_context_processor
def make_shell_context():
    return {'db':db, 'User':User, 'Posts':Posts}